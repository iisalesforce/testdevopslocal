#/usr/bin/env bash
# -lcommit builds last commit
# -prevrsa last commit to master
 
#read command line args
while getopts l:p: option
do
        case "${option}"
        in
                l) LCOMMIT=${OPTARG};;
                p) PREVRSA=${OPTARG};;
        esac
done
 
echo Last Commit: $LCOMMIT
echo Previous Commit: $PREVRSA
 

#cd force-app/main/default
pwd
echo changing directoy to force-app/main/default

echo List of changes
echo DIFF: `git diff-tree --no-commit-id --name-only --diff-filter=ACMRTUXB -t -r $PREVRSA $LCOMMIT`

#git diff-tree --no-commit-id --name-only --diff-filter=ACMRTUXB -t -r $PREVRSA $LCOMMIT | \

CFILES=""
for CFILE in `git diff-tree --no-commit-id --name-only --diff-filter=ACMRTUXB -t -r $PREVRSA $LCOMMIT `;
do
        echo $CFILE
        if [[ $CFILE == *"force-app/main/"*"."* ]]
        then
            CFILES=${CFILES:+$CFILES}$CFILE,
        fi
done

echo $CFILES

#remove last character of files
CFILES=${CFILES%?}
echo $CFILES 


#install sfdx
#curl “https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz” -o “sfdx.tar.xz”
#mkdir sfdx
#tar -xvJf sfdx.tar.xz >/dev/null 2>&1 -C sfdx —strip-components=1

/usr/local/bin/sfdx force:auth:jwt:grant --clientid 3MVG9aWdXtdHRrI2MhtOxtKYJMbvHEZ641e5HJ1zyv3AFr5YJrHZ9Ds2PoFnfYLqyKCkq2SMaRkSA8Ysq4LU0 --jwtkeyfile server.key --username gitadmin@tmbbank.com.devops2 --setalias devops2 --instanceurl https://test.salesforce.com
#deploy to org
/usr/local/bin/sfdx force:source:deploy -p $CFILES --verbose --loglevel fatal -u devops2
#echo "$CFILES"