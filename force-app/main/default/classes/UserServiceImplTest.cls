@isTest
public class UserServiceImplTest {
/*
    static testmethod void InactiveUsersServiceTest() {
        
        UserServiceImpl impl = new UserServiceImpl();
        
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
         User u = new User(Alias = 'stdus', Email='stdus@tmbbank.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_GB', ProfileId = p.Id,Employee_ID__c='12342',IsActive=true,
            TimeZoneSidKey='Asia/Bangkok', UserName='stdus@tmbbank.com');
        insert u;
        User u1 = new User(Alias = 'stdus1', Email='stdus1@tmbbank.com', 
            EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_GB', ProfileId = p.Id,Employee_ID__c='12343',IsActive=true,
            TimeZoneSidKey='Asia/Bangkok', UserName='stdus1@tmbbank.com');
        insert u1;
        Set<String> setRocodes = new Set<String> ();
		setRocodes.add('12342');
		setRocodes.add('12343');
		// Step 2) Call Factory Service
		Boolean ret = impl.InactiveUsersByROCodes(setRocodes);
       
        system.assertEquals(true, ret);
        
        //Check result
        List<User> result = [Select IsActive from User where Employee_ID__c IN:setRocodes];
        for(User rs: result){
           system.assertEquals(false, rs.IsActive); 
        }
		
    }
    
    
    static testmethod void CreateUsersServiceTest() {
        UserServiceImpl impl = new UserServiceImpl();
        //1 Case Create and Update
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
         User u = new User(Alias = '12346', Email='stdus@tmbbank.com',UserRoleId = [select Id from UserRole where Name = : 'CEO' ].Id, 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',  
            LocaleSidKey='en_GB', ProfileId = p.Id,Employee_ID__c='12346',IsActive=true,
            TimeZoneSidKey='Asia/Bangkok', UserName='12346@tmbbank.com');
        insert u;
        
        User u2 = new User(Alias = '24477', Email='aasdd@tmbbank.com',UserRoleId = [select Id from UserRole where Name = : 'CEO' ].Id, 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',  
            LocaleSidKey='en_GB', ProfileId = p.Id,Employee_ID__c='24477',IsActive=true,
            TimeZoneSidKey='Asia/Bangkok', UserName='24477@tmbbank.com');
        insert u2;
        
      
        
        UserInfoDTO dto = new UserInfoDTO();
        dto.Alias='12346'; 
        dto.Email='stdus2@tmbbank.com';
        dto.EmailEncodingKey='UTF-8';
        dto.firstName='stdus2'; 
        dto.LastName='Testing2'; 
        dto.LanguageLocaleKey='en_US';
        dto.LocaleSidKey='en_GB';
        dto.ProfileName = 'System Administrator';
        dto.UserRoleName ='SME Sales Management';
        dto.ROCode='12346';
        dto.IsActive='true';
        dto.TimeZoneSidKey='Asia/Bangkok';
        dto.UserName='12346@tmbbank.com';
        dto.DelegatedApproverId = String.Valueof([Select Id from User where IsActive=true Order by Name ASC  Limit 1 ].Id);
        //if(ManagerId!='')
        dto.ManagerId = '24477';
        system.debug('dto.ManagerId: '+dto.ManagerId);
        dto.UserPreferencesHideS1BrowserUI='false';
        dto.UserPermissionsSupportUser='false';
        dto.Segment='SE';
        dto.ReportingSegment='SE';
        dto.Region='SE';
        dto.RegionCode='9999';
        dto.Zone='SE';
        dto.ZoneCode='9999';
        dto.Phone='029393942';
        dto.MobilePhone='08992933';
        
        UserInfoDTO dto2 = new UserInfoDTO();
        dto2.Alias='12348'; 
        dto2.Email='stdus3@tmbbank.com';
        dto2.EmailEncodingKey='UTF-8';
        dto2.FirstName='stdus3'; 
        dto2.LastName='Testing3'; 
        dto2.LanguageLocaleKey='en_US';
        dto2.LocaleSidKey='en_GB';
        dto2.ProfileName = 'System Administrator';
        dto2.UserRoleName ='SME Sales Management';
        dto2.ROCode='12348';
        dto2.IsActive='true';
        dto2.TimeZoneSidKey='Asia/Bangkok';
        dto2.UserName='12348@tmbbank.com';
        dto2.DelegatedApproverId = String.Valueof([Select Id from User where IsActive= true Order by Name ASC Limit 1 ].Id);
        //if(ManagerId!='')
        dto2.ManagerId = '24477';
        system.debug('dto2.ManagerId: '+dto2.ManagerId);
        dto2.UserPreferencesHideS1BrowserUI='true';
        dto2.UserPermissionsSupportUser='true';
        dto2.Segment='SE';
        dto2.ReportingSegment='SE';
        dto2.Region='SE';
        dto2.RegionCode='9999';
        dto2.Zone='SE';
        dto2.ZoneCode='9999';
        dto2.Phone='029393942';
        dto2.MobilePhone='08992933';
        
        
        Set<UserInfoDTO> setRocodes = new Set<UserInfoDTO> ();
		setRocodes.add(dto);
		setRocodes.add(dto2);
		// Step 2) Call Factory Service
		Boolean ret = impl.CreateUsersByROCodes(setRocodes);
        system.debug('UserInfoDTO '+ret);
        system.assertEquals(true, ret);
        
        Set<String> setRocodes2 = new Set<String> ();
		setRocodes2.add('12346');
		setRocodes2.add('12348');
        //Check result
        List<User> result = [Select Employee_ID__c,IsActive,UserRole.Name,Alias,Email,LastName,UserPreferencesHideS1BrowserUI,UserPermissionsSupportUser,Username,EmailEncodingKey,LanguageLocaleKey,LocaleSidKey,TimeZoneSidKey from User where Employee_ID__c IN:setRocodes2];
        System.debug('Create Result: '+result);
        for(User rs: result){
            if(rs.Employee_ID__c=='12346'){
           	system.assertEquals(true, rs.IsActive);
            system.assertEquals('SME Sales Management', rs.UserRole.Name); 
            system.assertEquals('12346', rs.Alias);
            system.assertEquals('stdus2@tmbbank.com', rs.Email);
            system.assertEquals('Testing2', rs.LastName);
            system.assertEquals(false, rs.UserPreferencesHideS1BrowserUI);
            system.assertEquals(false, rs.UserPermissionsSupportUser);
            system.assertEquals('12346@tmbbank.com', rs.UserName);
            }else if(rs.Employee_ID__c=='12348'){
            system.assertEquals(true, rs.IsActive);
            system.assertEquals('SME Sales Management', rs.UserRole.Name);
            system.assertEquals('12348', rs.Alias);
            system.assertEquals('stdus3@tmbbank.com', rs.Email);
            system.assertEquals('Testing3', rs.LastName);
            system.assertEquals(false, rs.UserPreferencesHideS1BrowserUI);
            system.assertEquals(true, rs.UserPermissionsSupportUser);
            system.assertEquals('UTF-8', rs.EmailEncodingKey);
            system.assertEquals('en_US', rs.LanguageLocaleKey);
            system.assertEquals('en_GB', rs.LocaleSidKey);
            system.assertEquals('Asia/Bangkok', rs.TimeZoneSidKey);
            system.assertEquals('12348@tmbbank.com', rs.UserName);
            }
        }

    }
    
    
    static testmethod void InquiryUsersServiceTest() {
        UserServiceImpl impl = new UserServiceImpl();
        
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
         User u = new User(Alias = '12352', Email='stdus@tmbbank.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',UserRoleId = [select Id from UserRole where Name = : 'CEO' ].Id, 
            LocaleSidKey='en_GB', ProfileId = p.Id,Employee_ID__c='12352',IsActive=true,
            TimeZoneSidKey='Asia/Bangkok', UserName='12352@tmbbank.com',UserPreferencesHideS1BrowserUI=false,UserPermissionsSupportUser=false);
        insert u;
        User u1 = new User(Alias = '12353', Email='stdus1@tmbbank.com',UserRoleId = [select Id from UserRole where Name = : 'SME Sales Management'].Id, 
            EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_GB', ProfileId = p.Id,Employee_ID__c='12353',IsActive=false,
            TimeZoneSidKey='Asia/Bangkok', UserName='12353@tmbbank.com',UserPreferencesHideS1BrowserUI=true,UserPermissionsSupportUser=true);
        insert u1;
        Set<String> setRocodes = new Set<String> ();
		setRocodes.add('12352');
		setRocodes.add('12353');
		// Step 2) Call Factory Service
		List<UserInfoDTO> ret = impl.InquiryUsersByROCodes(setRocodes);
        system.debug('UserInfoDTO '+ret);
        system.assertEquals(2, ret.size());
        
     
        //Check result
        List<User> result = [Select Employee_ID__c,IsActive,UserRole.Name,Alias,Email,LastName,UserPreferencesHideS1BrowserUI,UserPermissionsSupportUser,Username,EmailEncodingKey,LanguageLocaleKey,LocaleSidKey,TimeZoneSidKey from User where Employee_ID__c IN:setRocodes];
        System.debug('Inquiry Result: '+result);
        for(User rs: result){
            if(rs.Employee_ID__c=='12352'){
           	system.assertEquals(true, rs.IsActive);
            system.assertEquals('CEO', rs.UserRole.Name); 
            system.assertEquals('12352', rs.Alias);
            system.assertEquals('stdus@tmbbank.com', rs.Email);
            system.assertEquals('Testing', rs.LastName);
            system.assertEquals(false, rs.UserPreferencesHideS1BrowserUI);
            system.assertEquals(false, rs.UserPermissionsSupportUser);
            system.assertEquals('12352@tmbbank.com', rs.UserName);
            }else if(rs.Employee_ID__c=='12353'){
            system.assertEquals(false, rs.IsActive);
            system.assertEquals('SME Sales Management', rs.UserRole.Name);
            system.assertEquals('12353', rs.Alias);
            system.assertEquals('stdus1@tmbbank.com', rs.Email);
            system.assertEquals('Testing1', rs.LastName);
            system.assertEquals(true, rs.UserPreferencesHideS1BrowserUI);
            system.assertEquals(true, rs.UserPermissionsSupportUser);
            system.assertEquals('UTF-8', rs.EmailEncodingKey);
            system.assertEquals('en_US', rs.LanguageLocaleKey);
            system.assertEquals('en_GB', rs.LocaleSidKey);
            system.assertEquals('Asia/Bangkok', rs.TimeZoneSidKey);
            system.assertEquals('12353@tmbbank.com', rs.UserName);
            }
        }
        
    }
    
    static testmethod void InquiryUsersServiceExceptionTest() {
        UserServiceImpl impl = new UserServiceImpl();
        Set<String> setRocodes = new Set<String> ();
		setRocodes.add('45646');
		setRocodes.add('62324');
		List<UserInfoDTO> ret = impl.InquiryUsersByROCodes(setRocodes);
        system.debug('UserInfoDTO '+ret);
    }*/
}