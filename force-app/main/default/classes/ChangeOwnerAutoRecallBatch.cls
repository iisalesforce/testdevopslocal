public class ChangeOwnerAutoRecallBatch implements Database.Batchable<sObject>, Database.Stateful {
    final String SEMICO = ',';
    final String jobDailyName = 'Change Owner - Auto Recall Approval And Approver Escalation';
	final String sObjectName = 'Change Owner Approval History';

    private Datetime batchDate = datetime.now();
	private Datetime batchStartTime = datetime.now();
	private Datetime batchEndTime = null;
    private Map<Id, String> mapEscalationResult;

    private Integer exceedRecallDays;
    private Integer exceedEscalateDays;
    private Integer escalateTotal;

    Datetime dateTimeNow;
    private BusinessHours bh;
    private Set<Id> changeOwnerIdSetRecall;
    private Map<Id, String> mapEscalateResult;
    
    public ChangeOwnerAutoRecallBatch() {
        System.debug(':::::::::::::::ChangeOwnerAutoRecallBatch:::::::::::::::');
        exceedRecallDays = Integer.valueOf(AppConfig__c.getValues('Auto Recall Change Owner Approval flow').Value__c);
        exceedEscalateDays = Integer.valueOf(AppConfig__c.getValues('Auto Reassign Current Approver').Value__c);

        dateTimeNow = Datetime.now();
        // Get the default business hours
        bh = [SELECT Id FROM BusinessHours WHERE IsDefault = true];

        mapEscalationResult = new Map<Id, String> ();

        escalateTotal = 0;

        changeOwnerIdSetRecall = new Set<Id>();
        mapEscalateResult = new Map<Id, String>();
    }

    public ChangeOwnerAutoRecallBatch(Integer month, Integer day) {
        System.debug(':::::::::::::::ChangeOwnerAutoRecallBatch:::::::::::::::');
        System.debug('month : ' + month);
        System.debug('day : ' + day);
        exceedRecallDays = Integer.valueOf(AppConfig__c.getValues('Auto Recall Change Owner Approval flow').Value__c);
        exceedEscalateDays = Integer.valueOf(AppConfig__c.getValues('Auto Reassign Current Approver').Value__c);
        dateTimeNow = Datetime.newInstance(Datetime.now().year(), month, day, Datetime.now().hour(), Datetime.now().minute(), Datetime.now().second());
        // Get the default business hours
        bh = [SELECT Id FROM BusinessHours WHERE IsDefault = true];

        mapEscalationResult = new Map<Id, String> ();

        escalateTotal = 0;

        changeOwnerIdSetRecall = new Set<Id>();
        mapEscalateResult = new Map<Id, String>();
    }


    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug(':::::::::::::::start:::::::::::::::');
        return Database.getQueryLocator([
            SELECT Id, Approval_Start_Wait_Time__c, Approval_Step_Start_Wait_Time__c, Change_Owner_Approval_Status__c, Approver1__c, Approver2__c, Approver3__c, Approver4__c
            FROM Change_Owner_Approval_History__c
            WHERE Change_Owner_Approval_Status__c IN ('Pending', 'The 1st Approver Approved', 'The 2nd Approver Approved', 'The 3rd Approver Approved') 
            AND Approval_Step_Start_Wait_Time__c <> NULL
            AND Approval_Start_Wait_Time__c <> NULL
        ]);
    }
    
    public void execute(Database.BatchableContext BC, List<Change_Owner_Approval_History__c> scope) {
        System.debug(':::::::::::::::execute:::::::::::::::');
       
        Map<Id, Change_Owner_Approval_History__c> mapScope = new Map<Id, Change_Owner_Approval_History__c> (scope);
        
        Set<Id> userIdset = new Set<Id> ();
        Map<Id, Change_Owner_Approval_History__c> mapChangeOwnerEscalate = new Map<Id, Change_Owner_Approval_History__c>  ();
        
        List<ProcessInstanceWorkitem> updatePiwlst = new List<ProcessInstanceWorkitem>();
        List<Change_Owner_Approval_History__c> updateChangeOwnHis = new List<Change_Owner_Approval_History__c>();

        System.debug('BusinessHours.isWithin(bh.Id, dateTimeNow)  : ' + BusinessHours.isWithin(bh.Id, dateTimeNow) );
        System.debug('!Test.isRunningTest() : ' + Test.isRunningTest());
         
        if(BusinessHours.isWithin(bh.Id, dateTimeNow) || Test.isRunningTest() ){
            List<User> userlst = new List<User>();
            for(Change_Owner_Approval_History__c changeownerHistory : scope){
                // System.debug(a.Id + ' |' + a.approval_start_wait_time__c + ' |' + BusinessHours.diff(bh.Id, a.approval_start_wait_time__c, dateTimeNow) / (24 * 60 * 60 * 1000) +'|'+ (BusinessHours.diff(bh.Id, a.approval_start_wait_time__c, dateTimeNow) > (exceedRecallDays * 24 * 60 * 60 * 1000)));
                System.debug('changeownerHistory : ' + changeownerHistory);

                System.debug('Recall : ' + changeownerHistory.Approval_Start_Wait_Time__c + ' : ' + dateTimeNow); 
                System.debug('Recall CURRENT : ' + BusinessHours.diff(bh.Id, changeownerHistory.Approval_Start_Wait_Time__c, dateTimeNow)); 
                System.debug('Recall CUT OFF : ' + (exceedRecallDays * 24 * 60 * 60 * 1000));

                System.debug('Escalate : ' + changeownerHistory.Approval_Step_Start_Wait_Time__c + ' : ' + dateTimeNow); 
                System.debug('Escalate CURRENT : ' + BusinessHours.diff(bh.Id, changeownerHistory.Approval_Step_Start_Wait_Time__c, dateTimeNow)); 
                System.debug('Escalate CUT OFF : ' + (exceedEscalateDays * 24 * 60 * 60 * 1000));

                if(changeownerHistory.Approver1__c != null)
                    userIdset.add(changeownerHistory.Approver1__c);
                if(changeownerHistory.Approver2__c != null)
                    userIdset.add(changeownerHistory.Approver2__c);
                if(changeownerHistory.Approver3__c != null)
                    userIdset.add(changeownerHistory.Approver3__c);
                if(changeownerHistory.Approver4__c != null)
                    userIdset.add(changeownerHistory.Approver4__c);

                if (BusinessHours.diff(bh.Id, changeownerHistory.Approval_Start_Wait_Time__c, dateTimeNow) >= (exceedRecallDays * 24 * 60 * 60 * 1000)) {
                    System.debug(':::: RECALL ::::');
                    changeOwnerIdSetRecall.add(changeownerHistory.Id);
                }else if(BusinessHours.diff(bh.Id, changeownerHistory.Approval_Step_Start_Wait_Time__c, dateTimeNow) >= (exceedEscalateDays * 24 * 60 * 60 * 1000)){
                    System.debug(':::: ESCALATE ::::');
                    escalateTotal += 1;
                    mapChangeOwnerEscalate.put(changeownerHistory.id, changeownerHistory);
                }
            }

            System.debug('userIdset : ' + userIdset);

            Map<Id, User> mapRecallUserCheck = new Map<Id, User> ([SELECT Id, IsActive FROM User WHERE Id IN :userIdset]);

            System.debug('mapRecallUserCheck : ' + mapRecallUserCheck);

            for(Change_Owner_Approval_History__c changeownerHistory : scope){
                Boolean criteria1 = (changeownerHistory.Approver1__c != null && !mapRecallUserCheck.get(changeownerHistory.Approver1__c).IsActive);
                Boolean criteria2 = (changeownerHistory.Approver2__c != null && !mapRecallUserCheck.get(changeownerHistory.Approver2__c).IsActive);
                Boolean criteria3 = (changeownerHistory.Approver3__c != null && !mapRecallUserCheck.get(changeownerHistory.Approver3__c).IsActive);
                Boolean criteria4 = (changeownerHistory.Approver4__c != null && !mapRecallUserCheck.get(changeownerHistory.Approver4__c).IsActive);
                
                Boolean MatchCriteriaRecall = criteria1 || criteria2 || criteria3 || criteria4;

                if(MatchCriteriaRecall){
                    changeOwnerIdSetRecall.add(changeownerHistory.Id);
                }
            }

            System.debug('mapChangeOwnerEscalate : ' + mapChangeOwnerEscalate);
            
            //ESCALATE PROCESS --- (RECALL PROCESS IN FINISH)
            Map<Id, String> result = ChangeOwnerAutoRecallBatchUtil.reassignApprover(mapChangeOwnerEscalate);

            for (Id recid : result.keySet()) {
                if(result.get(recid) == 'SalesManagement inactive'){
                    changeOwnerIdSetRecall.add(recid);
                    result.remove(recid);
                }
            }

            mapEscalateResult.putAll(result);
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        System.debug(':::::::::::::::finish:::::::::::::::');
        try{
            List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId IN :changeOwnerIdSetRecall];
            
            List<Approval.ProcessWorkitemRequest> reqList = new List<Approval.ProcessWorkitemRequest>();
            
            for (ProcessInstanceWorkitem p : piwi) {
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments('Auto recall performed by system');
                req.setAction('Removed');
                req.setWorkitemId(p.Id);
                reqList.add(req);
            }

            List<Approval.ProcessResult> resultProcess = Approval.process(reqList);
        }
        catch (Exception e){
            System.debug('System error: ' + e.getMessage());
            System.debug('There is error during processing : ' + e.getStackTraceString());
        }

        batchEndTime = datetime.now();

        Integer recordsSuccessful = escalateTotal - mapEscalateResult.size();
		Integer recordsFailed = mapEscalateResult.size();

        String csvHeaderStr = 'ChangeOwnerApprovalHistoryId, Result';
		String CSV_BODY  = '';
        String resultMessage;

        for(Id id : mapEscalateResult.keySet()){
            resultMessage = mapEscalateResult.get(id);
			System.debug(id + ' : ' + resultMessage);

            if(resultMessage != 'SUCCESS'){
                CSV_BODY = CSV_BODY + 
                        Id + SEMICO +
                        resultMessage + '\n';
            }
		}

        List<String> sendToEmail = new List<String>();
		sendToEmail.add('CRM Admin 2');

        System.debug(csvHeaderStr);
		System.debug(CSV_BODY);

        if(escalateTotal > 0){
			//SEND MAIL
			RTL_BatchEmailService.SendFinishBatchSummaryEmail(batchDate
                                                        , batchStartTime
                                                        , batchEndTime
                                                        , recordsSuccessful
                                                        , recordsFailed
														, csvHeaderStr
                                                        , jobDailyName
                                                        , sObjectName
                                                        , sendToEmail
                                                        , CSV_BODY);
		}
    }
}