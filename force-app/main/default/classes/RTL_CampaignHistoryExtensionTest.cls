@isTest
private class RTL_CampaignHistoryExtensionTest {

	@testSetup static void setupData() 
	{
        TestUtils.createAppConfig();
		RecordType rt = [SELECT id FROM recordType WHERE DeveloperName   = 'Exclusive_Campaign_Active' ];
		RecordType masterRt = [SELECT id,DeveloperName FROM recordType WHERE DeveloperName = 'Mass_Campaign_Active' ];


		RecordType localRt = [SELECT id,DeveloperName FROM recordType WHERE DeveloperName = 'Local_Exclusive_Campaign_Active' ];
		RecordType massRt = [SELECT id,DeveloperName FROM recordType WHERE DeveloperName = 'Mass_Campaign_Active' ];
        
		RTL_Campaign_Running_No__c rn = new RTL_Campaign_Running_No__c( 
                        Name ='Local Exclusive Campaign Running No.' , 
                        DateValue__c='170717',
                        Day__c='17',
                        Month__c='07',
                        Year__c='17',
                        Running_No__c = '01' );
        insert rn;

        RTL_CampaignMember_Running_No__c cmrn = new RTL_CampaignMember_Running_No__c(
        	Name = 'Campaign Member Running No',
        	Running_No__c = '000000'
        	);

        insert cmrn;

        List<AppConfig__c> mc = new List<AppConfig__c> ();
        mc.Add(new AppConfig__c(Name = 'runtrigger', Value__c = 'false'));
        insert mc;


        Branch_and_zone__c bz = new Branch_and_zone__c();
        bz.Branch_Code__c = '611';
        insert bz;
      
        User u = RTL_TestUtility.createUsers(1, 'UserTest' ,'User1' , 'test@email.com' , false).get(0);
        u.RTL_Branch_Code__c  = '611';
        insert u;
        


        Data_Security_Matrix__c dsm = new Data_Security_Matrix__c(Section_Name__c = 'RtlCust:Campaign History', 
                                                                  Profile_Name__c = 'System Administrator', 
                                                                  Access_Type__c = 'All');
		insert dsm;


		List<Campaign> camList = new List<Campaign>();
        Campaign masterC = RTL_TestUtility.createMasterCampaign('MasterCam1',
        	'D20171001',
        	'Exclusive',
        	'MB',u
        	);


		camList.add(masterC);

		Campaign masterCamCusReq = RTL_TestUtility.createMasterCampaign('MasterCam2 Cust Req',
        	'D20171002',
        	'Exclusive',
        	'MB',u
        	);
		camList.add(masterCamCusReq);

		//Campaign masterLocalReq = RTL_TestUtility.createMasterCampaign('MasterCam3 Local',
  //      	'L20171002',
  //      	'Local Exclusive',
  //      	'MB',u
  //      	);
		//camList.add(masterLocalReq);

		Campaign masterMassReq = RTL_TestUtility.createMasterCampaign('MasterCam4 Mass',
        	'M20171002',
        	'Mass',
        	'MB',u
        	);
		camList.add(masterMassReq);

		insert camList;

		List<Campaign> childCamList = new List<Campaign>();
		Campaign childC = RTL_TestUtility.createChildCampaign('ChildCam1','1', masterC , rt);
		childCamList.add(childC);

		Campaign childC2 = RTL_TestUtility.createChildCampaign('ChildCam2 Cust Request','2', masterCamCusReq , rt) ;
		childCamList.add(childC2);

		//Campaign childC3 = RTL_TestUtility.createChildCampaign('ChildCam3 Local','3', masterCamCusReq , localRt) ;
		//childCamList.add(childC3);

		Campaign childC4 = RTL_TestUtility.createChildCampaign('ChildCam4 Mass','2', masterMassReq , massRt) ;
		childCamList.add(childC4);

		insert childCamList;

		CampaignMemberStatus cms = new CampaignMemberStatus();
		cms.CampaignId = masterCamCusReq.id;
		cms.SortOrder = 9;
		cms.Label = 'aaa';
		insert cms;

		CampaignMemberStatus cms2 = new CampaignMemberStatus();
		cms2.CampaignId = childC2.id;
		cms2.SortOrder = 8;
		cms2.Label = 'aaa';
		insert cms2;

		masterCamCusReq.RTL_Category__c = 'Sales'; 
		masterCamCusReq.RTL_Campaign_Objective__c = 'Drop-Lead';
		update camList;

		List<Lead> leadList = new List<Lead>();
        Lead l = new Lead(Company = 'JohnMiller', LastName = 'Mike', Status = 'Open');
        leadList.add(l);

        insert leadList;


        List<CampaignMember> cml = new List<CampaignMember>();

        CampaignMember cm1 = new CampaignMember();
        cm1.LeadId = l.id;
        cm1.CampaignId = childC.id;
        cml.add(cm1);



        Account acct = RTL_TestUtility.createAccounts(1 , true).get(0);

        Contact ct = RTL_TestUtility.createContacts(acct);
        ct.OwnerId = u.id;
        insert ct;

        CampaignMember cm2 = new CampaignMember();
        cm2.ContactId = ct.id;
        cm2.CampaignId = childC.id;
        cml.add(cm2);


        //====================== Customer Request =============================

        CampaignMember cm3 = new CampaignMember();
        cm3.ContactId = ct.id;
        cm3.CampaignId = childC2.id;
        cml.add(cm3);

        //===================== Local Exclusive ==============================

        //CampaignMember cm4 = new CampaignMember();
        //cm3.ContactId = ct.id;
        //cm3.CampaignId = childC3.id;
        //cml.add(cm4);

        //===================== Mass ==================================

        CampaignMember cm5 = new CampaignMember();
        cm5.ContactId = ct.id;
        cm5.CampaignId = childC4.id;
        cml.add(cm5);
       
        insert cml;

	}

	@isTest static void test_method_one() {
        
		Test.startTest();

			Account acc = [SELECT ID FROM Account LIMIT 1];
	        PageReference pageRef = Page.RTL_CampaignHistory;
	        pageRef.getParameters().put('id', String.valueOf(acc.Id));
	        Test.setCurrentPage(pageRef);

        	ApexPages.StandardController sc = new ApexPages.StandardController(acc);
			RTL_CampaignHistoryExtension che = new RTL_CampaignHistoryExtension(sc);

	        che.navigateLocalEx();
	        che.nextLocalEx();
	        che.previousLocalEx();

	        che.navigateMass();
	        che.nextMass();
	        che.previousMass();
        
        	che.navigateExclusive();

	        che.campaignPeriodMethod  = 'past';
	        che.setCampaignPeriod();
        
        	che.reloadAllData();

	        string campaignLiteURL  = che.campaignLiteURL;
	        boolean isRenderCampaign  = che.isRenderCampaign;
	        boolean isRenderSMS  = che.isRenderSMS;

        Test.stopTest();
	}
	


	@isTest static void test_method_two() {


        Test.startTest();

        	Account acc = [SELECT ID FROM Account LIMIT 1];

	        PageReference pageRef = Page.RTL_CampaignHistory;
	        pageRef.getParameters().put('id', String.valueOf(acc.Id));
	        pageRef.getParameters().put('tabName', 'fulfillment');
	        Test.setCurrentPage(pageRef);

        	ApexPages.StandardController sc = new ApexPages.StandardController(acc);
			RTL_CampaignHistoryExtension che = new RTL_CampaignHistoryExtension(sc);

	        che.navigateLocalEx();
	        che.nextLocalEx();
	        che.previousLocalEx();

	        che.navigateMass();
	        che.nextMass();
	        che.previousMass();

	        che.previousFulfillment();
	        che.nextFulfillment();
	        che.navigateFulfillment();
        
	        che.previousSMS();
	        che.nextSMS();
	        che.navigateSMS();
        

        	che.navigateExclusive();
        	che.nextExclusive();
        	che.previousExclusive();
        	che.navigateCustomerRequest();
        	che.nextCustomerRequest();
        	che.previousCustomerRequest();

	        che.campaignPeriodMethod  = 'current';
	        che.setCampaignPeriod();
        	
        	che.displayData();

        	pageRef.getParameters().put('tabName', 'sms');
	        Test.setCurrentPage(pageRef);
        	che.displayData();

        	pageRef.getParameters().put('tabName', 'campaign');
	        Test.setCurrentPage(pageRef);
	        che.displayData();

        Test.stopTest();
	}

	@isTest static void test_method_three() {

		List<Integer> totalPageList =  new List<Integer>();
		Integer clickPage = 10;
		for(Integer i =0;i<20;i++){
			totalPageList.add(i);
		}
        Test.startTest();
        
        	Account acc = [SELECT ID FROM Account LIMIT 1];

        	ApexPages.StandardController sc = new ApexPages.StandardController(acc);
			RTL_CampaignHistoryExtension che = new RTL_CampaignHistoryExtension(sc);
	        che.pagination(totalPageList,clickPage);
	        che.reloadAllData();
	        che.requestInt06();
	        che.processResponseInt06();
        Test.stopTest();
	}
	
}