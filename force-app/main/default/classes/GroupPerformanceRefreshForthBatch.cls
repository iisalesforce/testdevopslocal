public without sharing class GroupPerformanceRefreshForthBatch {}
/*Comment Cleansing Code global without sharing class GroupPerformanceRefreshForthBatch implements Database.Batchable < sObject >, Database.AllowsCallouts {

	// Account Plan Year
	public string m_year { get; set; }
	//  Support
	public Set<Id> m_accountWithAccountPlan { get; set; }
	public Set<Id> m_accountWithoutAccountPlan { get; set; }

	public id m_groupId { get; set; }
	public id m_groupProfileId { get; set; }

	global GroupPerformanceRefreshForthBatch(Set<Id> accountWithAccountPlan, Set<Id> accountWithoutAccountPlan, id groupId, string year, id groupProfileId) {

		m_accountWithAccountPlan = accountWithAccountPlan;
		m_accountWithoutAccountPlan = accountWithoutAccountPlan;
		m_year = year;
		m_groupId = groupId;
		m_groupProfileId = groupProfileId;
	}
	// Start Method
	global Database.QueryLocator start(Database.BatchableContext BC) {
		//Get Account to Process
		Set<Id> accountIds = m_accountWithAccountPlan;
		string year = m_year;

		return Database.getQueryLocator([
		                                SELECT Id, Name, Account_Plan_Flag__c, Group__c, Group__r.Name, Owner.Segment__c
		                                FROM Account
		                                WHERE Id IN :accountIds
		                                ]);
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		Set<id> accountIds = (new Map<Id, SObject> (scope)).keySet();
		AccountPlanRefreshService.RefreshGroupPerformance(accountIds, m_year);
	}


	global void finish(Database.BatchableContext BC) {

		string mainUrl = 'https://' + System.URL.getSalesforceBaseUrl().getHost() + '/' + 'apex/AccountplanGroupPerformanceV2?walletID=&CompanyID=&GroupID=' + m_groupProfileId;

		string htmlMsg = 'Refresh Group Performance batch processing is completed'
		+ '<br />Please click below url to view group formance '
		+ '<br />View <a href="' + mainUrl + '"> click here</a>';



		BatchEmailService.SendEmail(BC.getJobId(), 'Batch Processing', 'Refresh Group Performance batch processing is completed', htmlMsg);
		AcctPlanGroupWalletLockService.Unlock(m_groupId);
	}
}*/