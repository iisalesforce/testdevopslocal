@isTest
public class RTL_LeadTriggerHandlerTest {
    static List<Account> acctList;
    static List<Lead> leadList;
    static Map<String, Group> queueMap;
    static {
        TestUtils.createAppConfig();       
    }
    
    static testmethod void testLeadConversion(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            RTL_TestUtility.createRetailMasterProducts(true);
            leadList = RTL_TestUtility.createLeads(2,true);
            RTL_TestUtility.createInterestedProducts(leadList, true);
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(2, true);
            
            leadList[0].Status = 'Qualified';
            update leadList;

            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(leadList[0].id);
            lc.setDoNotCreateOpportunity(false);
            lc.setConvertedStatus('Closed Converted');

            Database.LeadConvertResult lcr = Database.convertLead(lc);
            System.assert(lcr.isSuccess());
        }
        Test.stopTest();
    }
    
    static testmethod void testLeadConversionFail(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            RTL_TestUtility.createRetailMasterProducts(true);
            leadList = RTL_TestUtility.createLeads(2,true);
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(2, true);
            
            

            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(leadList[0].id);
            lc.setDoNotCreateOpportunity(false);
            lc.setConvertedStatus('Closed Converted');
            
            try{
                Database.LeadConvertResult lcr = Database.convertLead(lc);
            }catch(Exception e){
                
            }
            RTL_TestUtility.createInterestedProducts(leadList, true);
            leadList[0].Status = 'Qualified';
            update leadList;

        }
        Test.stopTest();
    }
    
    static testmethod void testChangeLeadOwner(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        User retailUser2 = [select ID from user where isactive = true and ProfileId in (SELECT Id FROM Profile WHERE Name = 'TMB Retail Channel Sales' ) LIMIT 1];
        System.runAs(retailUser) {
            RTL_TestUtility.createRetailMasterProducts(true);
            leadList = RTL_TestUtility.createLeads(2,true);
            RTL_TestUtility.createInterestedProducts(leadList, true);
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(2, true);
    
        }
        leadList[0].OwnerId = retailUser2.id;       
        update leadList;
        Test.stopTest();
    }
    
    static testmethod void testChangeLeadOwnerQueue(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            queueMap = RTL_TestUtility.createLeadQueues(true);
            RTL_TestUtility.createRetailMasterProducts(true);
            leadList = RTL_TestUtility.createLeads(2,true);
            RTL_TestUtility.createInterestedProducts(leadList, true);
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(2, true);
            leadList[0].OwnerId = queueMap.get('RTLQ_001').id;
            update leadList;
        }        
        Test.stopTest();
    }
    
    static testmethod void testChangeLeadOwnerOutboundQueue(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            queueMap = RTL_TestUtility.createLeadQueues(true);
            RTL_TestUtility.createRetailMasterProducts(true);
            leadList = RTL_TestUtility.createLeads(2,true);
            RTL_TestUtility.createInterestedProducts(leadList, true);
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(2, true);
            leadList[0].OwnerId = queueMap.get('RTLQ_Outbound').id;
            update leadList;
    
        }
  
        Test.stopTest();
    }

    public static testmethod void testWebDropLead(){
        System.debug(':::: testWebDropLead Start ::::');
        RTL_TestUtility.enableWebToLeadAutoAssign(true); 
        RTL_TestUtility.createCampaignAssignmentRule();
        RTL_TestUtility.createOpportunityRecordTypeMapping();
        
        List<RTL_product_master__c> productList = new List<RTL_product_master__c>();
        
        RTL_product_master__c product = new RTL_product_master__c(Name='So Smart Test', Product_Group__c='Credit Card & RDC', Product_Sub_group__c='Credit Card',Active__c=true);
        productList.add(product);

        insert productList;
        
         RTL_CampaignMember_Running_No__c cmrn = new RTL_CampaignMember_Running_No__c(
            Name = 'Campaign Member Running No',
            Running_No__c = '000000'
        );
        
        insert cmrn;

        //create test lead queue
        Map<String, Group> leadQueues = RTL_TestUtility.createLeadQueues(true);

        //create test branch
        RTL_TestUtility.createBranchZone(1, true);
        //create test product and web-to-lead assignment criterias
        RTL_TestUtility.createLeadAssignCriterias(true);
        

        TEST.startTest();     

        for(Integer i =0 ; i < 20 ; i++){
            Lead lead = new Lead();
            lead.Company = 'X';
            lead.RTL_TMB_Campaign_Source__c = 'Web';
            lead.FirstName = 'Test1';
            lead.LastName = 'WebToLead';
            lead.Email = 'test1@salesforce.com';
            lead.City = 'Bangkok';
            lead.RTL_Mobile_Number__c = '0819803882';
            lead.RTL_BranchW2L_Area__c = '10400';
            lead.RTL_Branch_Name__c = '001';
            lead.LeadSource = 'Others';
            lead.RTL_Product_Name__c = 'So Smart Test';
            lead.RTL_TMB_Campaign_Reference__c = 'CMN_web';
            lead.RTL_Customer_Segment__c = 'TMB Web';
            lead.RTL_Web_Unique_ID_c__c  = '20171120'+i;
            lead.RecordTypeId = '01290000000iZYKAA2';
            /////lead.RTL_Multiple_Interested_Product_s__c = true;
            insert lead;
        }
        //
        //RTL_TestUtility.createPositiveWebToLead();
        List<Lead> webtolead = [select RTL_Count_InterestedProducts_Primary__c from Lead ];
        System.debug('web to lead ::: '+ webtolead);
        
        TEST.stopTest();
        System.debug(':::: testWebDropLead End ::::');    
    }

    public static testmethod void testUpdatePrimaryInterestProduct(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        
        System.runAs(retailUser) {
            //create branch and zone
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(3, true);            
            //create retail master product
            RTL_TestUtility.createRetailMasterProducts(true);
            //create new test lead with primary interested product
            Lead lead = RTL_TestUtility.createLeads2(true);
            //check result: the count of primary interested product should be 1
            Lead selectlead = [SELECT RTL_Primary_Interested_Product__c, RTL_Count_InterestedProducts_Primary__c FROM Lead WHERE Id=:lead.Id];
            System.assertEquals(1, selectlead.RTL_Count_InterestedProducts_Primary__c);

            Test.setCurrentPage(Page.RetailLeadEdit);
            ApexPages.StandardController stdSetController = new ApexPages.StandardController(lead);
            // stdSetController.setSelected(lead);
            RetailLeadExtension ext = new RetailLeadExtension(stdSetController);
            ext.saveLead();
        
            // Update RTL Primary Interested Product on Lead Object / Master Code 004
            lead = RTL_TestUtility.updateLeads2(lead, '0004', false);
            stdSetController = new ApexPages.StandardController(lead);
            ext = new RetailLeadExtension(stdSetController);
            ext.saveLead();
            RTL_product_master__c masterPrimaryProduct = [SELECT Id, Product_Code__c, Product_Group__c, Product_Sub_group__c
                FROM RTL_product_master__c 
                WHERE Product_Code__c = : '0004' AND Active__c = : true LIMIT 1];
            selectlead = [SELECT Id, RTL_Primary_Interested_Product__c, RTL_Count_InterestedProducts_Primary__c FROM Lead WHERE Id=:lead.Id];
            System.assertEquals(masterPrimaryProduct.id, selectlead.RTL_Primary_Interested_Product__c);

            // // Remove RTL Interest Product on Lead Object
            // lead.RTL_Primary_Interested_Product__c = null;
            // stdSetController = new ApexPages.StandardController(lead);
            // ext = new RetailLeadExtension(stdSetController);
            // ext.saveLead();
            // selectlead = [SELECT Id, RTL_Primary_Interested_Product__c, RTL_Count_InterestedProducts_Primary__c FROM Lead WHERE Id=:lead.Id];
            // System.assertEquals(0, selectlead.RTL_Count_InterestedProducts_Primary__c);

            // Update RTL Primary Interested Product on Lead Object / Master Code 002
            lead = RTL_TestUtility.updateLeads2(lead, '0002', false);
            stdSetController = new ApexPages.StandardController(lead);
            ext = new RetailLeadExtension(stdSetController);
            ext.saveLead();
            masterPrimaryProduct = [SELECT Id, Product_Code__c, Product_Group__c, Product_Sub_group__c
                FROM RTL_product_master__c 
                WHERE Product_Code__c = : '0002' AND Active__c = : true LIMIT 1];
            selectlead = [SELECT Id, RTL_Primary_Interested_Product__c, RTL_Count_InterestedProducts_Primary__c FROM Lead WHERE Id=:lead.Id];
            System.assertEquals(masterPrimaryProduct.id, selectlead.RTL_Primary_Interested_Product__c);
        }
        Test.stopTest();
    }

    public static testmethod void testUpdatePrimaryInterestProduct2(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        
        System.runAs(retailUser) {
            //create branch and zone
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(3, true);            
            //create retail master product
            RTL_TestUtility.createRetailMasterProducts(true);
            //create new test lead with primary interested product
            Lead lead = RTL_TestUtility.createLeads2(true);
            //check result: the count of primary interested product should be 1
            Lead selectlead = [SELECT RTL_Primary_Interested_Product__c, RTL_Count_InterestedProducts_Primary__c FROM Lead WHERE Id=:lead.Id];
            System.assertEquals(1, selectlead.RTL_Count_InterestedProducts_Primary__c);

            Test.setCurrentPage(Page.RetailLeadEdit);
            ApexPages.StandardController stdSetController = new ApexPages.StandardController(lead);
            // stdSetController.setSelected(lead);
            RetailLeadExtension ext = new RetailLeadExtension(stdSetController);
            ext.saveLead();
        
            // Remove RTL Interest Product on Lead Object
            lead.RTL_Primary_Interested_Product__c = null;
            stdSetController = new ApexPages.StandardController(lead);
            ext = new RetailLeadExtension(stdSetController);
            ext.saveLead();
            selectlead = [SELECT Id, RTL_Primary_Interested_Product__c, RTL_Count_InterestedProducts_Primary__c FROM Lead WHERE Id=:lead.Id];
            System.assertEquals(0, selectlead.RTL_Count_InterestedProducts_Primary__c);

            // Update RTL Primary Interested Product on Lead Object / Master Code 002
            lead = RTL_TestUtility.updateLeads2(lead, '0002', false);
            stdSetController = new ApexPages.StandardController(lead);
            ext = new RetailLeadExtension(stdSetController);
            ext.saveLead();
            RTL_product_master__c masterPrimaryProduct = [SELECT Id, Product_Code__c, Product_Group__c, Product_Sub_group__c
                FROM RTL_product_master__c 
                WHERE Product_Code__c = : '0002' AND Active__c = : true LIMIT 1];
            selectlead = [SELECT Id, RTL_Primary_Interested_Product__c, RTL_Count_InterestedProducts_Primary__c FROM Lead WHERE Id=:lead.Id];
            System.assertEquals(masterPrimaryProduct.id, selectlead.RTL_Primary_Interested_Product__c);
        }
        Test.stopTest();
    }

    public static testmethod void testLeadMgmDuplicate(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        
        System.runAs(retailUser) {
            // Create Lead
            Lead lead = new Lead();
            // lead.Company = 'X';
            // lead.RTL_TMB_Campaign_Source__c = 'Web';
            lead.FirstName = 'Test1';
            lead.LastName = 'WebToLead';
            lead.Email = 'test1@salesforce.com';
            // lead.City = 'Bangkok';
            lead.RTL_Mobile_Number__c = '0987456123';
            // lead.RTL_BranchW2L_Area__c = '10400';
            // lead.RTL_Branch_Name__c = '001';
            lead.LeadSource = 'Other';
            // lead.RTL_Product_Name__c = 'So Smart Test';
            // lead.RTL_TMB_Campaign_Reference__c = 'CMN_web';
            // lead.RTL_Customer_Segment__c = 'TMB Web';
            lead.RTL_Web_Unique_ID_c__c  = '201711201';
            lead.RecordTypeId = '01290000000iZYKAA2';
            insert lead;

            System.debug('Lead Id: ' + lead.Id);

            try{
                // Create duplicate Lead.
                Lead leadDup = new Lead();
                // leadDup.Company = 'X2';
                // leadDup.RTL_TMB_Campaign_Source__c = 'Web';
                leadDup.FirstName = 'Test2';
                leadDup.LastName = 'WebToLead';
                leadDup.Email = 'test2@salesforce.com';
                leadDup.City = 'Bangkok';
                // Same Mobile Number
                leadDup.RTL_Mobile_Number__c = '0987456123';
                // leadDup.RTL_BranchW2L_Area__c = '10400';
                // leadDup.RTL_Branch_Name__c = '001';
                // LeadSource "CM Referral"
                leadDup.LeadSource = 'CM Referral';
                // leadDup.RTL_Product_Name__c = 'So Smart Test';
                // leadDup.RTL_TMB_Campaign_Reference__c = 'CMN_web';
                // leadDup.RTL_Customer_Segment__c = 'TMB Web';
                leadDup.RTL_Web_Unique_ID_c__c  = '201711202';
                // Record Type "Retial Banking"
                leadDup.RecordTypeId = '01290000000iZYKAA2';
                insert leadDup;

                System.debug('leadDup Id: ' + leadDup.Id);
            }catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains(System.Label.Lead_MGM_Duplicate_with_Existing) ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
        }
        Test.stopTest();
    }

}