@isTest
global class TMBAccountPlanServiceProxyMock /*implements WebServiceMock*/ {
/*	public void doInvoke(
	                     Object stub,
	                     Object request,
	                     Map<String, Object> response,
	                     String endpoint,
	                     String soapAction,
	                     String requestName,
	                     String responseNS,
	                     String responseName,
	                     String responseType) {
		System.debug(LoggingLevel.INFO, 'TMBServiceProxyMockImpl.doInvoke() - ' +
		             '\n request: ' + request +
		             '\n response: ' + response +
		             '\n endpoint: ' + endpoint +
		             '\n soapAction: ' + soapAction +
		             '\n requestName: ' + requestName +
		             '\n responseNS: ' + responseNS +
		             '\n responseName: ' + responseName +
		             '\n responseType: ' + responseType);



		TMBAccountPlanServiceProxy.QueryCustomerInfos_element request_x = (TMBAccountPlanServiceProxy.QueryCustomerInfos_element) request;


		List<String> accts = new List<String>();

		try
		{
			String[] ar = request_x.strOfIds.split(',');

			for (string strid : ar)
			{
				string obj =  strid;
				System.debug(':::: Id = ' + obj);
				accts.add(obj);
			}
		}
		catch(Exception e)
		{
			System.debug(e);
			
		} 



		// Return Message
		TMBAccountPlanServiceProxy.QueryCustomerInfosResponse_element response_x = new TMBAccountPlanServiceProxy.QueryCustomerInfosResponse_element();
		// Mock Result 
		TMBAccountPlanServiceProxy.CustomerInfoDTO queryCustomerInfosResult = new TMBAccountPlanServiceProxy.CustomerInfoDTO();
		queryCustomerInfosResult.status = '0000';
		queryCustomerInfosResult.totalrecord = accts.size()+'';
		queryCustomerInfosResult.massage = '';
		// Records return
		TMBAccountPlanServiceProxy.ArrayOfCUSTOMER_INFO arrayOfCustomerInfo = new TMBAccountPlanServiceProxy.ArrayOfCUSTOMER_INFO();
		TMBAccountPlanServiceProxy.CUSTOMER_INFO[] customerInfos = new List<TMBAccountPlanServiceProxy.CUSTOMER_INFO> ();
		//  Record 1
		for (string item : accts)
		{
			TMBAccountPlanServiceProxy.CUSTOMER_INFO data1 = new TMBAccountPlanServiceProxy.CUSTOMER_INFO();
			data1.SF_ID = item;
			data1.CRM_ID = item;
			data1.TMB_CUST_ID = item;
			data1.FNAME = 'XXXXX'+item;
			data1.LNAME = 'XXXXX'+item;
			System.debug('::: data ' +  data1);
			// Insert to List
			customerInfos.add(data1);
		}
		arrayOfCustomerInfo.CUSTOMER_INFO = customerInfos;
		queryCustomerInfosResult.Datas = arrayOfCustomerInfo;



		response_x.QueryCustomerInfosResult = queryCustomerInfosResult;
		response.put('response_x', response_x);
	}*/
}