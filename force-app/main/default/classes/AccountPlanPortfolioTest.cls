@isTest
global class AccountPlanPortfolioTest {}
/* Comment Clean Code
    static{
        TestUtils.createAppConfig();
        TestUtils.createStatusCode(); 
        TestUtils.createDisqualifiedReason(); 
        TestUtils.CreateAddress();
        AccountPlanTestUtilities.getAcctPlanMode();
    }
    static testmethod void AccountPlanPortfolioInitiate(){
		
        User SalesOwner = AccountPlanTestUtilities.createUsers(1, 'RM', 'PortfolioInitiateTest', 'portfolio@test.com', AccountPlanTestUtilities.TMB_RM_PROFILE_ID,false, true).get(0);
        
        List<Account> AccountList = AccountPlanTestUtilities.createAccounts(5, 'InitiateTest', 'Individual', SalesOwner.id, true, true);
        Group__c mastergroup = AccountPlanTestUtilities.createGroupMaster(1,'Initiatetest', false, true).get(0);
        for(account acct  : AccountList){
            acct.Group__c =mastergroup.id;
        }
        update AccountList;
        AccountPlanTestUtilities.createAccounts(1, 'InitiateNonGroupTest', 'Individual', SalesOwner.id, true, true);
        List<Target__c> TaragetList = AccountPlanTestUtilities.createTargetNI(5,  SalesOwner.id, true);
       
        System.runAs(SalesOwner){
        AcctPlanPortfolio__c portfolio = AccountPlanTestUtilities.createAccountPlanPortfolio(SalesOwner.id, '2014', 10000000, false);
        ApexPages.StandardController sc = new ApexPages.StandardController(portfolio);
       
        
        AccountPlanPortfolioInitiateCtrl portfolioCtrl = new AccountPlanPortfolioInitiateCtrl(sc);
            portfoliocTrl.portfolio = portfolio;
             Test.setMock(WebServiceMock.class, new TMBAccountPlanServiceProxyMock());
            Test.startTest();
             
            
            portfolioCtrl.getFiscalYear();
            portfolioCtrl.selectedYear();
            portfoliocTrl.portfolio.Year__c = '2015';
            portfolioCtrl.Refreshfunction();
            portfolioCtrl.save();
               
            portfolioCtrl.save();   
            portfolioCtrl.FiltersOption ='Target';
        	portfolioCtrl.AccountPlanFilters();
            portfolioCtrl.FiltersOption ='Flag';
            portfolioCtrl.AccountPlanFilters();
            portfolioCtrl.FiltersOption ='Group';
            portfolioCtrl.AccountPlanFilters();
            
			Test.stopTest();            
        }
        
    }
    
    static testmethod void AccountPlanPortfolioManagement(){
         User SalesOwner = AccountPlanTestUtilities.createUsers(1, 'RM', 'PortfolioMngTest', 'portfoliomng@test.com', AccountPlanTestUtilities.TMB_RM_PROFILE_ID,false, true).get(0);
        
        List<Account> AccountList = AccountPlanTestUtilities.createAccounts(5, 'PortMngTest', 'Individual', SalesOwner.id, true, true);
        AccountPlanTestUtilities.createCompanyProfileByAccount(AccountList, true);
        Group__c mastergroup = AccountPlanTestUtilities.createGroupMaster(1,'PortMngtest', false, true).get(0);
        List<group__c> mgroupList = new List <group__c>();
        mgroupList.add(mastergroup);
        AccountPlanTestUtilities.createGroupProfilebyGroup(mgroupList,true);
        for(account acct  : AccountList){
            acct.Group__c =mastergroup.id;
        }
        update AccountList;
        AccountList.add(AccountPlanTestUtilities.createAccounts(1, 'PortMngNonGroupTest', 'Individual', SalesOwner.id, true, true).get(0));
        List<Target__c> TaragetList = AccountPlanTestUtilities.createTargetNI(5,  SalesOwner.id, true);
       
        
        //Addmore
         List<Account> AccountNewList = AccountPlanTestUtilities.createAccounts(2, 'AddPortMngTest', 'Individual', SalesOwner.id, true, true);
            Group__c Newmastergroup = AccountPlanTestUtilities.createGroupMaster(1,'AddPortMngtest', false, true).get(0);
            for(account acct  : AccountNewList){
                acct.Group__c =Newmastergroup.id;
            }
        	AccountNewList.get(0).Group__c = mastergroup.id;
            update AccountNewList;
            AccountNewList.add(AccountPlanTestUtilities.createAccounts(1, 'AddPortMngNonGroupTest', 'Individual', SalesOwner.id, true, true).get(0));
      		
         
        
        
        
        
        System.runAs(SalesOwner){
            
            
         
             AcctPlanPortfolio__c portfolio = AccountPlanTestUtilities.createAccountPlanPortfolio(SalesOwner.id, '2014', 10000000, true);

            List<Group__c> groupmasterlist = new List<Group__c>();
            groupmasterlist.add(mastergroup);
        List<AcctPlanGroupPort__c> groupportlist = AccountPlanTestUtilities.createGroupPortbyGroupMaster(groupmasterlist, portfolio, true);
        List<AcctPlanCompanyPort__c> comportList = AccountPlanTestUtilities.createCompanyPortByAccount(AccountList, portfolio, true);
        
      
               PageReference PortfolioPage = Page.AccountPlanPortfolioManagement;
               PortfolioPage.getParameters().put('id',portfolio.id);
               Test.setCurrentPage(PortfolioPage);
       
             Test.setMock(WebServiceMock.class, new TMBAccountPlanServiceProxyMock());
            
            Test.startTest();
            
            
            
            
             ApexPages.StandardController sc = new ApexPages.StandardController(portfolio);
             AccountPlanPortfolioManagementCtrl portfolioCtrl = new AccountPlanPortfolioManagementCtrl(sc);
           
            for(AcctPlanCompanyPort__c comport : comportList){
                comport.Target_NI_By_RM__c =10000000;
                comport.Wallet__c = 200000;
                comport.Target_NI_By_RM__c = 300000;
                comport.Performance__c = 400000;
                comport.ExpectedNIbyAccountPlan__c = 500000;
            }
            update comportlist;
            portfolioCtrl.FiltersOption ='Target';
             portfolioCtrl.AccountPlanFilters();
            portfolioCtrl.FiltersOption ='Flag';
            portfolioCtrl.AccountPlanFilters();
            portfolioCtrl.FiltersOption ='Group';
             portfolioCtrl.AccountPlanFilters();
            AccountPlanPortfolioManagementCtrl.ApprovalWrapper appWrap = new AccountPlanPortfolioManagementCtrl.ApprovalWrapper();
            
            portfolioCtrl.Refreshfunction();
            portfolioCtrl.RefreshWalletFunction();
            portfolioCtrl.updateTargetNI();
        	 
            PortfolioPage.getParameters().put('acctKey',comportList.get(0).id);
            portfolioCtrl.InitiateAccountPlan();
            portfolioCtrl.InitiateAccountPlan();
            portfolioCtrl.EditAccountPlan();
            
            AcctPlanCompanyPort__c comportNegative = comportlist.get(5);
            comportNegative.Account_Name__c = null;
            comportNegative.AcctPlanGroupPort__c = null;
            update comportNegative;
            PortfolioPage.getParameters().put('acctKey',comportList.get(5).id);
            portfolioCtrl.InitiateAccountPlan();
            portfolioCtrl.EditAccountPlan();
            
            
            portfolioCtrl.ApprovalExRemoteAction();
            portfolioCtrl.ApprovalEx();
            portfolioCtrl.redirect();
           Test.stopTest();
        }
        
       
           
         
        
    }*/
 	
    /*static testmethod void AccountPlanPortfolioManagementEdit(){
         User SalesOwner = AccountPlanTestUtilities.createUsers(1, 'RM', 'PortfolioMngTest', 'portfoliomng@test.com', AccountPlanTestUtilities.TMB_RM_PROFILE_ID,false, true).get(0);
        
        List<Account> AccountList = AccountPlanTestUtilities.createAccounts(5, 'PortMngTest', 'Individual', SalesOwner.id, true, true);
        AccountPlanTestUtilities.createCompanyProfileByAccount(AccountList, true);
        Group__c mastergroup = AccountPlanTestUtilities.createGroupMaster(1,'PortMngtest', false, true).get(0);
        List<group__c> mgroupList = new List <group__c>();
        mgroupList.add(mastergroup);
        AccountPlanTestUtilities.createGroupProfilebyGroup(mgroupList,true);
        for(account acct  : AccountList){
            acct.Group__c =mastergroup.id;
        }
        update AccountList;
        AccountList.add(AccountPlanTestUtilities.createAccounts(1, 'PortMngNonGroupTest', 'Individual', SalesOwner.id, true, true).get(0));
        List<Target__c> TaragetList = AccountPlanTestUtilities.createTargetNI(5,  SalesOwner.id, true);
       
        
        //Addmore
         List<Account> AccountNewList = AccountPlanTestUtilities.createAccounts(2, 'AddPortMngTest', 'Individual', SalesOwner.id, true, true);
            Group__c Newmastergroup = AccountPlanTestUtilities.createGroupMaster(1,'AddPortMngtest', false, true).get(0);
            for(account acct  : AccountNewList){
                acct.Group__c =Newmastergroup.id;
            }
        	AccountNewList.get(0).Group__c = mastergroup.id;
            update AccountNewList;
            AccountNewList.add(AccountPlanTestUtilities.createAccounts(1, 'AddPortMngNonGroupTest', 'Individual', SalesOwner.id, true, true).get(0));
      		
         
        
        
        
        
        System.runAs(SalesOwner){
            
            
         
             AcctPlanPortfolio__c portfolio = AccountPlanTestUtilities.createAccountPlanPortfolio(SalesOwner.id, '2014', 10000000, true);

            List<Group__c> groupmasterlist = new List<Group__c>();
            groupmasterlist.add(mastergroup);
        List<AcctPlanGroupPort__c> groupportlist = AccountPlanTestUtilities.createGroupPortbyGroupMaster(groupmasterlist, portfolio, true);
        List<AcctPlanCompanyPort__c> comportList = AccountPlanTestUtilities.createCompanyPortByAccount(AccountList, portfolio, true);
        
         
      
               PageReference PortfolioPage = Page.AccountPlanPortfolioManagement;
               PortfolioPage.getParameters().put('id',portfolio.id);
               Test.setCurrentPage(PortfolioPage);
       
             Test.setMock(WebServiceMock.class, new TMBAccountPlanServiceProxyMock());
            
            Test.startTest();
            
            
            
            
            ApexPages.StandardController sc = new ApexPages.StandardController(portfolio);
            AccountPlanPortfolioManagementEditCtrlV2 portfolioEditCtrl = new AccountPlanPortfolioManagementEditCtrlV2(sc);
            portfolioEditCtrl.AccountPlanFilters();
            portfolioEditCtrl.FiltersOption ='Flag';
            portfolioEditCtrl.AccountPlanFilters();
            portfolioEditCtrl.FiltersOption ='Group';
            portfolioEditCtrl.AccountPlanFilters();
            
            for(AcctPlanCompanyPort__c comport : comportList){
                comport.Target_NI_By_RM__c =10000000;
            }
            update comportlist;
            portfolioEditCtrl.updateTargetNI();
        	 
            
            
            PortfolioPage.getParameters().put('acctKey',comportList.get(0).id);
            portfolioEditCtrl.redirect();
           Test.stopTest();
        }
        
       
           
         
        
    }
    */
    /* Comment Clean Code
    static testmethod void AccountPlanPortfolioApproval(){
         User SalesOwner = AccountPlanTestUtilities.createUsers(1, 'RM', 'PortfolioMngTest', 'portfoliomng@test.com', AccountPlanTestUtilities.TMB_RM_PROFILE_ID,false, true).get(0);
         AcctPlanPortfolio__c portfolio = AccountPlanTestUtilities.createAccountPlanPortfolio(SalesOwner.id, '2014', 10000000, true);
         List<Account> AccountList = AccountPlanTestUtilities.createAccounts(1, 'ApprovalTest', 'Individual', SalesOwner.id, true, true);
         AcctPlanCompanyProfile__c comprofile = AccountPlanTestUtilities.createCompanyProfileByAccount(AccountList, true).get(0);
        
        
        
        
       			PageReference PortfolioPage = Page.AccountPlanPortfolioApproval;
               PortfolioPage.getParameters().put('id',portfolio.id);
        	   PortfolioPage.getParameters().put('action','Request');
               Test.setCurrentPage(PortfolioPage);
        		ApexPages.StandardController sc = new ApexPages.StandardController(portfolio);
             	AccountPlanPortfolioApproval approvalCtrl = new AccountPlanPortfolioApproval(sc);
        		approvalCtrl.tempport.SalesOwner__c = SalesOwner.id;
        		approvalCtrl.ProcessResult();
        		approvalCtrl.approveRecord();
        		approvalCtrl.ProcessResult();
        		approvalCtrl.rejectRecord();
        		approvalCtrl.ProcessResult();
        		approvalCtrl.recallrecord();
        		approvalCtrl.getWorkItemId(portfolio.id);
        
        	  PageReference Approval = Page.AccountPlanPortfolioApproval;
               Approval.getParameters().put('ComId',comprofile.id);
        	   Approval.getParameters().put('action','Request');
               Test.setCurrentPage(Approval);
        		ApexPages.StandardController ScCompro = new ApexPages.StandardController(comprofile);
             	AccountPlanPortfolioApproval approvalCtrl2 = new AccountPlanPortfolioApproval(ScCompro);
       
    }
    
    static testmethod void AccountPlanPortfolioProcessStep(){
        User SalesOwner = AccountPlanTestUtilities.createUsers(1, 'RM', 'PortfolioMngTest', 'portfoliomng@test.com', AccountPlanTestUtilities.TMB_RM_PROFILE_ID,false, true).get(0);
         
         AcctPlanPortfolio__c portfolio = AccountPlanTestUtilities.createAccountPlanPortfolio(SalesOwner.id, '2014', 10000000, true);


        // Create an approval request for the account
        Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(portfolio.id); 
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        // Submit on behalf of a specific submitter
        req1.setSubmitterId(SalesOwner.id); 
        // Submit the record to specific process and skip the criteria evaluation
        req1.setProcessDefinitionNameOrId('Account_Plan_Portfolio_Approval_Flow');
        req1.setSkipEntryCriteria(true);
        Approval.ProcessResult result = Approval.process(req1);
        
        
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        // Instantiate the new ProcessWorkitemRequest object and populate it

        Approval.ProcessWorkitemRequest req2 =
            new Approval.ProcessWorkitemRequest();
        req2.setComments('Approving request.');
        req2.setAction('Approve');
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(newWorkItemIds.get(0));
        // Submit the request for approval
        Approval.ProcessResult result2 =  Approval.process(req2);
        
       
          
        
        	   PageReference PortfolioPage = Page.AccountPlanPortfolioManagement;
               PortfolioPage.getParameters().put('id',portfolio.id);
               Test.setCurrentPage(PortfolioPage);
        
          	 ApexPages.StandardController sc = new ApexPages.StandardController(portfolio);
             AccountPlanPortfolioManagementCtrl portfolioCtrl = new AccountPlanPortfolioManagementCtrl(sc);
    }
 
   
}*/