@isTest
public class ChangeOwnerApproverControllerTest {
    public static final Id SESM_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB SE Sales Management Profile' LIMIT 1].Id;
    public static final Id RPSPEC_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB Commercial Report Specialist Profile' LIMIT 1].Id;
    public static final Id TH_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB Team Head Profile' LIMIT 1].Id;
    public static final Id SEZM_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB SE Zone Manager Profile' LIMIT 1].Id;
    public static final Id BDM_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB BDM Profile' LIMIT 1].Id;

    static{
        TestUtils.createAppConfig();
        // TestUtils.createAccounts(1, 'name', 'Qualified Prospect', true);

        Branch_and_zone__c bz = new Branch_and_zone__c();
        bz.Branch_Code__c = '001';
        insert bz;

        List<User> userlst = new List<User>();

        User User00 = RTL_TestUtility.createUsers(1, 'testuser0', 'testuser0', 'testuser0@tmb.com.test', false).get(0);
		User00.ProfileId = SEZM_PROFILE_ID;
		User00.isActive = true;
		User00.RTL_Branch_Code__c = bz.Branch_Code__c;
		User00.Employee_ID__c = '000000';
		User00.Zone__c = 'SE-ChiangMai';
		User00.Zone_Code__c = '9999';
		User00.Region__c = 'SE1-North1';
		User00.Region_Code__c = '1111';
        userlst.add(User00);
		
        User User10 = RTL_TestUtility.createUsers(1, 'testuser10', 'testuser10', 'testuser10@tmb.com.test', false).get(0);
		User10.ProfileId = RPSPEC_PROFILE_ID;
		User10.isActive = true;
		User10.RTL_Branch_Code__c = bz.Branch_Code__c;
		User10.Employee_ID__c = '111000';
		User10.Zone__c = 'SE-ChiangMai';
		User10.Zone_Code__c = '9999';
		User10.Region__c = 'SE1-North1';
		User10.Region_Code__c = '1111';
        userlst.add(User10);

        insert userlst;
        userlst = new List<User>();

        User User01 = RTL_TestUtility.createUsers(1, 'testuser', 'testuser', 'testuser@tmb.com.test', false).get(0);
		User01.ProfileId = SESM_PROFILE_ID;
		User01.isActive = true;
		User01.RTL_Branch_Code__c = bz.Branch_Code__c;
		User01.Employee_ID__c = '111111';
		User01.Zone__c = 'SE-ChiangMai';
		User01.Zone_Code__c = '9999';
		User01.Region__c = 'SE1-North1';
		User01.Region_Code__c = '1111';
        User01.Segment__c = 'SE';
        userlst.add(User01);

        User User04 = RTL_TestUtility.createUsers(1, 'testuser4', 'testuser4', 'testuser4@tmb.com.test', false).get(0);
		User04.ProfileId = BDM_PROFILE_ID;
		User04.isActive = true;
		User04.RTL_Branch_Code__c = bz.Branch_Code__c;
		User04.Employee_ID__c = '444444';
		User04.Zone__c = 'SE-ChiangMai';
		User04.Zone_Code__c = '9999';
		User04.Region__c = 'SE1-North1';
		User04.Region_Code__c = '1111';
        User04.Segment__c = 'SE';
        User04.ManagerId = User00.Id;
        userlst.add(User04);

        User User05 = RTL_TestUtility.createUsers(1, 'testuser5', 'testuser5', 'testuser5@tmb.com.test', false).get(0);
		User05.ProfileId = BDM_PROFILE_ID;
		User05.isActive = true;
		User05.RTL_Branch_Code__c = bz.Branch_Code__c;
		User05.Employee_ID__c = '555555';
		User05.Zone__c = 'SE-ChiangMai';
		User05.Zone_Code__c = '8888';
		User05.Region__c = 'SE1-North1';
		User05.Region_Code__c = '2222';
        User05.Segment__c = 'SE';
        User05.ManagerId = User00.Id;
        userlst.add(User05);

        User User06 = RTL_TestUtility.createUsers(1, 'testuser6', 'testuser6', 'testuser6@tmb.com.test', false).get(0);
		User06.ProfileId = BDM_PROFILE_ID;
		User06.isActive = false;
		User06.RTL_Branch_Code__c = bz.Branch_Code__c;
		User06.Employee_ID__c = '666666';
		User06.Zone__c = 'BB-ChiangMai';
		User06.Zone_Code__c = '8888';
		User06.Region__c = 'BB-North1';
		User06.Region_Code__c = '2222';
        User06.Segment__c = 'BB';
        User06.ManagerId = User00.Id;
        userlst.add(User06);
        
        User User07 = RTL_TestUtility.createUsers(1, 'testuser7', 'testuser7', 'testuser7@tmb.com.test', false).get(0);
		User07.ProfileId = BDM_PROFILE_ID;
		User07.isActive = true;
		User07.RTL_Branch_Code__c = bz.Branch_Code__c;
		User07.Employee_ID__c = '777777';
		User07.Zone__c = 'BB-ChiangMai';
		User07.Zone_Code__c = '8888';
		User07.Region__c = 'BB-North1';
		User07.Region_Code__c = '2222';
        User07.Segment__c = 'BB';
        User07.ManagerId = User00.Id;
        userlst.add(User07);
                    
      	User User071 = RTL_TestUtility.createUsers(1, 'testuser71', 'testuser71', 'testuser71@tmb.com.test', false).get(0);
		User071.ProfileId = BDM_PROFILE_ID;
		User071.isActive = true;
		User071.RTL_Branch_Code__c = bz.Branch_Code__c;
		User071.Employee_ID__c = '777111';
		User071.Zone__c = 'BB-ChiangMai';
		User071.Zone_Code__c = '8888';
		User071.Region__c = 'BB-North1';
		User071.Region_Code__c = '2222';
        User071.Segment__c = 'BB';
        User071.ManagerId = User10.Id;
        userlst.add(User071);

        User User08 = RTL_TestUtility.createUsers(1, 'testuser8', 'testuser8', 'testuser8@tmb.com.test', false).get(0);
		User08.ProfileId = BDM_PROFILE_ID;
		User08.isActive = true;
		User08.RTL_Branch_Code__c = bz.Branch_Code__c;
		User08.Employee_ID__c = '888888';
		User08.Zone__c = 'BB-ChiangMai';
		User08.Zone_Code__c = '8888';
		User08.Region__c = 'BB-North1';
		User08.Region_Code__c = '2222';
        User08.Segment__c = 'BB';
        User08.ManagerId = User00.Id;
        userlst.add(User08);

        User User09 = RTL_TestUtility.createUsers(1, 'testuser9', 'testuser9', 'testuser9@tmb.com.test', false).get(0);
		User09.ProfileId = BDM_PROFILE_ID;
		User09.isActive = true;
		User09.RTL_Branch_Code__c = bz.Branch_Code__c;
		User09.Employee_ID__c = '999999';
		User09.Zone__c = 'BB-ChiangMai';
		User09.Zone_Code__c = '8888';
		User09.Region__c = 'BB-North1';
		User09.Region_Code__c = '2222';
        User09.Segment__c = 'ME';
        User09.ManagerId = User00.Id;
        userlst.add(User09);

        insert userlst;

        List<Account> existingAccts = TestUtils.createAccounts(4, 'name', 'Existing Customer', false);
        
        existingAccts.get(0).OwnerId = User04.Id;
        existingAccts.get(0).Account_Type__c = 'Existing Customer';
        existingAccts.get(0).Stamp_Owner_Region_Group__c = 'SE1-North1';
        existingAccts.get(0).Stamp_Owner_Region_Code_Group_Code__c = '1111';
        existingAccts.get(0).Stamp_Owner_Zone_Team__c = 'SE-ChiangMai';
        existingAccts.get(0).Stamp_Owner_Zone_Code_Team_C__c = '9999';
        existingAccts.get(0).Stamp_Owner_Segment__c = 'SE';

        existingAccts.get(1).OwnerId = User01.Id;
        existingAccts.get(1).Account_Type__c = 'Existing Customer';
        existingAccts.get(1).Stamp_Owner_Region_Group__c = 'SE1-North1';
        existingAccts.get(1).Stamp_Owner_Region_Code_Group_Code__c = '1111';
        existingAccts.get(1).Stamp_Owner_Zone_Team__c = 'SE-ChiangMai';
        existingAccts.get(1).Stamp_Owner_Zone_Code_Team_C__c = '9999';
        existingAccts.get(1).Stamp_Owner_Segment__c = 'SE';

        existingAccts.get(2).OwnerId = User07.Id;
        existingAccts.get(2).Account_Type__c = 'Existing Customer';
        existingAccts.get(2).Stamp_Owner_Region_Group__c = 'BB-North1';
        existingAccts.get(2).Stamp_Owner_Region_Code_Group_Code__c = '1111';
        existingAccts.get(2).Stamp_Owner_Zone_Team__c = 'BB-ChiangMai';
        existingAccts.get(2).Stamp_Owner_Zone_Code_Team_C__c = '9999';
        existingAccts.get(2).Stamp_Owner_Segment__c = 'BB';
                    
        existingAccts.get(3).OwnerId = User071.Id;
        existingAccts.get(3).Account_Type__c = 'Existing Customer';
        existingAccts.get(3).Stamp_Owner_Region_Group__c = 'BB-North1';
        existingAccts.get(3).Stamp_Owner_Region_Code_Group_Code__c = '1111';
        existingAccts.get(3).Stamp_Owner_Zone_Team__c = 'BB-ChiangMai';
        existingAccts.get(3).Stamp_Owner_Zone_Code_Team_C__c = '9999';
        existingAccts.get(3).Stamp_Owner_Segment__c = 'BB';
        
        insert existingAccts;

        List<Change_Owner_Approval_History__c> changeOwnerHistorylst = new List<Change_Owner_Approval_History__c> ();

        Change_Owner_Approval_History__c changeOwnerHistory = new Change_Owner_Approval_History__c();
        changeOwnerHistory.Account__c = existingAccts[0].Id;
        changeOwnerHistory.Approver1__c = User04.Id;
        changeOwnerHistory.Approver1_Profile__c = User04.Profile.Name;
        changeOwnerHistory.Approver2__c = User01.Id;
        changeOwnerHistory.Approver2_Profile__c = User01.Profile.Name;
        changeOwnerHistory.Change_to_owner__c = User05.Id;
        changeOwnerHistory.Current_Owner__c = existingAccts[0].OwnerId; 
        changeOwnerHistory.Change_Owner_Remark__c = 'preFixremark' + ' : ' ;
        changeOwnerHistory.Change_Owner_Approval_Status__c = 'The 3rd Approver Approved';
        changeOwnerHistorylst.add(changeOwnerHistory);

        Change_Owner_Approval_History__c changeOwnerHistory2 = new Change_Owner_Approval_History__c();
        changeOwnerHistory2.Account__c = existingAccts[0].Id;
        changeOwnerHistory2.Approver1__c = User04.Id;
        changeOwnerHistory2.Approver1_Profile__c = User04.Profile.Name;
        changeOwnerHistory2.Approver2__c = User01.Id;
        changeOwnerHistory2.Approver2_Profile__c = User01.Profile.Name;
        changeOwnerHistory2.Change_to_owner__c = User06.Id;
        changeOwnerHistory2.Current_Owner__c = existingAccts[0].OwnerId; 
        changeOwnerHistory2.Change_Owner_Remark__c = 'preFixremark' + ' : ' ;
        changeOwnerHistory2.Change_Owner_Approval_Status__c = 'The 3rd Approver Approved';
        changeOwnerHistorylst.add(changeOwnerHistory2);

        insert changeOwnerHistorylst;
    }

    @isTest
    public static void testAfterUpdateTrigger() {
        List<Change_Owner_Approval_History__c> currentChangeOwnerApproval = [SELECT Id FROM Change_Owner_Approval_History__c WHERE Change_Owner_Approval_Status__c IN ('Pending', 'The 1st Approver Approved', 'The 2nd Approver Approved', 'The 3rd Approver Approved') LIMIT 2];

		Test.startTest();
        currentChangeOwnerApproval[0].Change_Owner_Approval_Status__c = 'Final Approved';
        currentChangeOwnerApproval[1].Change_Owner_Approval_Status__c = 'Final Approved';
        update currentChangeOwnerApproval;

        User user = [SELECT Id, Name FROM User WHERE Email = 'testuser7@tmb.com.test' AND ProfileId = :BDM_PROFILE_ID LIMIT 1];
        Account acct = [SELECT Id FROM Account WHERE Owner.Email = :'testuser4@tmb.com.test' AND Owner.ProfileId = :BDM_PROFILE_ID  LIMIT 1];

        System.runAs(user){
            ApexPages.StandardController sc = new ApexPages.StandardController(acct);
            PageReference pageRef = Page.RequestChangeOwnerAccountPage;
            Test.setCurrentPage(pageRef);
            ChangeOwnerApproverController ctrl = new ChangeOwnerApproverController(sc);

            PageReference confirmApprover = ctrl.confirmApprover();
            PageReference cancelAction = ctrl.cancelAction();
        }

        Test.stopTest();
    }

    @isTest
    public static void testOwnerSpecialProfile() {

        User user = [SELECT Id, Name FROM User WHERE Email = 'testuser5@tmb.com.test' AND ProfileId = :BDM_PROFILE_ID LIMIT 1];
        Account acct = [SELECT Id FROM Account WHERE Owner.Email = :'testuser@tmb.com.test' AND Owner.ProfileId = :SESM_PROFILE_ID  LIMIT 1];

		Test.startTest();
        System.runAs(user){
            ApexPages.StandardController sc = new ApexPages.StandardController(acct);
            PageReference pageRef = Page.RequestChangeOwnerAccountPage;
            Test.setCurrentPage(pageRef);
            ChangeOwnerApproverController ctrl = new ChangeOwnerApproverController(sc);
        }

        Test.stopTest();
    }
    
    @isTest
    public static void testWBGTOWBG() {

        User user = [SELECT Id, Name FROM User WHERE Email = 'testuser8@tmb.com.test' AND ProfileId = :BDM_PROFILE_ID LIMIT 1];
        Account acct = [SELECT Id FROM Account WHERE Owner.Email = :'testuser7@tmb.com.test' AND Owner.ProfileId = :BDM_PROFILE_ID  LIMIT 1];

		Test.startTest();
        System.runAs(user){
            ApexPages.StandardController sc = new ApexPages.StandardController(acct);
            PageReference pageRef = Page.RequestChangeOwnerAccountPage;
            Test.setCurrentPage(pageRef);
            ChangeOwnerApproverController ctrl = new ChangeOwnerApproverController(sc);
        }

        Test.stopTest();
    }
    
    @isTest
    public static void testWBGTOWBGSM() {

        User user = [SELECT Id, Name FROM User WHERE Email = 'testuser8@tmb.com.test' AND ProfileId = :BDM_PROFILE_ID LIMIT 1];
        Account acct = [SELECT Id FROM Account WHERE Owner.Email = :'testuser71@tmb.com.test' AND Owner.ProfileId = :BDM_PROFILE_ID  LIMIT 1];

		Test.startTest();
        System.runAs(user){
            ApexPages.StandardController sc = new ApexPages.StandardController(acct);
            PageReference pageRef = Page.RequestChangeOwnerAccountPage;
            Test.setCurrentPage(pageRef);
            ChangeOwnerApproverController ctrl = new ChangeOwnerApproverController(sc);
        }

        Test.stopTest();
    }

    @isTest
    public static void testOwnerisCurrentUser() {

        User user = [SELECT Id, Name FROM User WHERE Email = 'testuser7@tmb.com.test' AND ProfileId = :BDM_PROFILE_ID LIMIT 1];
        Account acct = [SELECT Id FROM Account WHERE Owner.Email = :'testuser7@tmb.com.test' AND Owner.ProfileId = :BDM_PROFILE_ID  LIMIT 1];

		Test.startTest();
        System.runAs(user){
            ApexPages.StandardController sc = new ApexPages.StandardController(acct);
            PageReference pageRef = Page.RequestChangeOwnerAccountPage;
            Test.setCurrentPage(pageRef);
            ChangeOwnerApproverController ctrl = new ChangeOwnerApproverController(sc);
        }

        Test.stopTest();
    }

    @isTest
    public static void testSEtoWBG() {

        User user = [SELECT Id, Name FROM User WHERE Email = 'testuser5@tmb.com.test' AND ProfileId = :BDM_PROFILE_ID LIMIT 1];
        Account acct = [SELECT Id FROM Account WHERE Owner.Email = :'testuser7@tmb.com.test' AND Owner.ProfileId = :BDM_PROFILE_ID  LIMIT 1];

		Test.startTest();
        System.runAs(user){
            ApexPages.StandardController sc = new ApexPages.StandardController(acct);
            PageReference pageRef = Page.RequestChangeOwnerAccountPage;
            Test.setCurrentPage(pageRef);
            ChangeOwnerApproverController ctrl = new ChangeOwnerApproverController(sc);
        }

        Test.stopTest();
    }

    @isTest
    public static void testMEtoBB() {

        User user = [SELECT Id, Name FROM User WHERE Email = 'testuser9@tmb.com.test' AND ProfileId = :BDM_PROFILE_ID LIMIT 1];
        Account acct = [SELECT Id FROM Account WHERE Owner.Email = :'testuser7@tmb.com.test' AND Owner.ProfileId = :BDM_PROFILE_ID  LIMIT 1];

		Test.startTest();
        System.runAs(user){
            ApexPages.StandardController sc = new ApexPages.StandardController(acct);
            PageReference pageRef = Page.RequestChangeOwnerAccountPage;
            Test.setCurrentPage(pageRef);
            ChangeOwnerApproverController ctrl = new ChangeOwnerApproverController(sc);
        }

        Test.stopTest();
    }

    @isTest
    public static void addAppconfig(){
        List<AppConfig__c> Aconfiglst = new List<AppConfig__c>();
        
        AppConfig__c Aconfig1 = new AppConfig__c();
        Aconfig1.Name = 'RM/BDM Profile';
        Aconfig1.Value__c = 'TMB ABDM Profile;TMB BDM Profile;TMB RM Profile;TMB RMA Profile;TMB BB RM Profile;TMB BB ARM Profile';        
        Aconfiglst.add(Aconfig1);

        AppConfig__c Aconfig2 = new AppConfig__c();
        Aconfig2.Name = 'Custbase Approver Profile';
        Aconfig2.Value__c = 'TMB SE Zone Manager Profile;TMB SE Regional Manager Profile;TMB Team Head Profile;TMB BB Regional Manager Profile';        
        Aconfiglst.add(Aconfig2);

        insert Aconfiglst;

        Test.startTest();
        Account acct = [SELECT Id FROM Account LIMIT 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(acct);
        PageReference pageRef = Page.RequestChangeOwnerAccountPage;
        Test.setCurrentPage(pageRef);
        ChangeOwnerApproverController ctrl = new ChangeOwnerApproverController(sc);
        Test.stopTest();
    }
}