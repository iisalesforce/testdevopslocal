public class ChangeOwnerApprovalHistoryTriggerHandler {
    public static void handleAfterUpdate(List<Change_Owner_Approval_History__c> listNew, List<Change_Owner_Approval_History__c> listOld){
        Map<Id, Change_Owner_Approval_History__c> mapOld = new Map<Id, Change_Owner_Approval_History__c>(listOld);
        Map<Id, Change_Owner_Approval_History__c> mapNew = new Map<Id, Change_Owner_Approval_History__c>(listNew);

        Map<Id, Change_Owner_Approval_History__c> mapAcctChangeOwnerHis = new Map<Id, Change_Owner_Approval_History__c>();

        Map<Id, String> updateOwnerResult = new Map<Id, String>();
        
        for(Change_Owner_Approval_History__c changeOwnerAppHis : listNew){
            Boolean criteria1 = changeOwnerAppHis.Change_Owner_Approval_Status__c == 'Final Approved';
            Boolean criteria2 = changeOwnerAppHis.Change_Owner_Cross_Segment_flag__c == false;
            Boolean criteria3 = changeOwnerAppHis.Change_Owner_Approval_Status__c != mapOld.get(changeOwnerAppHis.Id).Change_Owner_Approval_Status__c;
            Boolean criteria4 = changeOwnerAppHis.Change_to_owner__c != null;

            Boolean criteria = criteria1 && criteria2 && criteria3 && criteria4;

            
            if(criteria){
                mapAcctChangeOwnerHis.put(changeOwnerAppHis.Account__c, changeOwnerAppHis);
            }
        }

        if(mapAcctChangeOwnerHis.size() > 0){
            updateOwnerResult = updateAccountOwner(mapAcctChangeOwnerHis);

            if(updateOwnerResult != null && updateOwnerResult.size() > 0){
                List<Change_Owner_Approval_History__c> changeOwnerHislst = [SELECT Id, Change_Owner_Remark__c FROM Change_Owner_Approval_History__c WHERE Id IN :updateOwnerResult.keySet()];
                
                for(Change_Owner_Approval_History__c changeOwnerAppHis : changeOwnerHislst){
                    changeOwnerAppHis.Change_Owner_Remark__c = updateOwnerResult.get(changeOwnerAppHis.id);
                }

                if(changeOwnerHislst != null && changeOwnerHislst.size() > 0){
                    update changeOwnerHislst;
                }
            }
        }

    }

    public static Map<Id, String> updateAccountOwner(Map<Id, Change_Owner_Approval_History__c> mapAcctChangeOwnerHis){
        Set<id> acctIdSet = mapAcctChangeOwnerHis.keySet();
        Map<Id, String> updateOwnerResult = new Map<Id, String>();
        List<Account> acctLst = [SELECT Id, OwnerId FROM Account WHERE Id IN :acctIdSet];

        if(acctLst.size() > 0){
            for(Account acct : acctLst){
                acct.OwnerId = mapAcctChangeOwnerHis.get(acct.Id).Change_to_owner__c;
            }

            if(acctLst != null && acctLst.size() > 0){
                List<Database.SaveResult> srList = Database.update(acctLst, false);

                for (Integer i = 0 ; i < srList.size() ; i++){
                    Database.SaveResult sr = srList.get(i);
                    
                    if (!sr.isSuccess()) {
                        String message = 'Can not Change Owner due to Requester is inactive.';

                        for(Database.Error err : sr.getErrors()) {
                            // message += err.getStatusCode() + ':' + err.getMessage();
                        }

                        // message += ' | ';

                        updateOwnerResult.put(mapAcctChangeOwnerHis.get(acctLst.get(i).Id).Id, message);
                    }
                }
            }
        }
        return updateOwnerResult;
    }
}