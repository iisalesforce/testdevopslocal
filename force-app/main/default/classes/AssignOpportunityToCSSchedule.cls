global class AssignOpportunityToCSSchedule implements Schedulable {
	global void execute(SchedulableContext sc) {
        
        String q = 'SELECT Id, Name, LeadSource, StageName, CreatedDate '+ 
                    'FROM Opportunity '+
                    'Where StageName IN (\'Submit to Credit Process\') AND LeadSource = \'Lazada\' AND CreatedDate = TODAY'; 
        
		AssignOpportunityToCSBatch b = new AssignOpportunityToCSBatch(q);
		Database.executebatch(b);
	}
}