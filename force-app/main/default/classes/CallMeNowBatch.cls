global class CallMeNowBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful {
	
	String query;
	List<Id> referralIdList;
	List<RTL_Online_Service_Log__c> logList;
	List<RTL_Referral__c> referralList;
	
	private String name;
	private String referralId;
	private String firstName;
	private String lastName;
	private String interestedProduct;
	private String mobile;
	private String campaign;
	private String subProduct;
	private String channel;

	private BusinessHours bh;
	
	//CR Refer from branch to any channel
	private String referralRecordtypeName;
	
	global CallMeNowBatch(List<Id> referralIdList) {
		logList = new List<RTL_Online_Service_Log__c>();
		referralList = new List<RTL_Referral__c>();

		bh = [SELECT Id FROM BusinessHours WHERE Name = 'Call Me Now'];
		
		this.referralIdList = referralIdList;
		query = 'SELECT Id,Name,RTL_FirstName__c,RTL_LastName__c,RTL_RecordType_Name__c,RTL_Product_Name_Str__c,RTL_Interested_Product__c,RTL_Sub_Product__c,RTL_Mobile1__c,RTL_Channel_Segment__c,RTL_Campaign__c,RTL_Call_Me_Now_Request_Count__c FROM RTL_Referral__c WHERE ID IN : referralIdList';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<CallMeNowDTO> callMeNowList = new List<CallMeNowDTO>();
		Datetime dateTimeNow = Datetime.now();

		for(sObject obj : scope){
			RTL_Referral__c referral = (RTL_Referral__c)obj;
			
			if(!BusinessHours.isWithin(bh.Id, dateTimeNow)){
				referral.RTL_Call_Me_Now_Status__c = 'fail';
			}else{
				CallMeNowDTO callMeNowObj;

				name = referral.Name;
				referralId = referral.Id;
				firstName = referral.RTL_FirstName__c;
				lastName = referral.RTL_LastName__c;
				referralRecordtypeName = referral.RTL_RecordType_Name__c;

				//CR Refer from branch to any channel
				if (referralRecordtypeName == 'Retail Cross Channel Referral') {
					interestedProduct = referral.RTL_Product_Name_Str__c;
				}else{
					interestedProduct = referral.RTL_Interested_Product__c;
				}

				mobile = referral.RTL_Mobile1__c;
				campaign = referral.RTL_Campaign__c;
				subProduct = referral.RTL_Sub_Product__c;
				channel = referral.RTL_Channel_Segment__c;

				callMeNowObj = RTL_ReferralAssignUtility.doCallService(referralId,name,firstName,lastName,interestedProduct,mobile,campaign,subProduct,channel,'true',referralRecordtypeName);
				referral.RTL_Call_Me_Now_Request__c = callMeNowObj.generateJSONContent();
				referral.RTL_Call_Me_Now_Response__c = callMeNowObj.response.rawResponse;
				referral.RTL_Call_Me_Now_Status__c = 'success';
				referral.RTL_From_Call_Me_Now__c = true;
				
				if(callMeNowObj.response.message != 'success'){
					referral.RTL_Call_Me_Now_Status__c = 'fail';
					referral.RTL_Call_Me_Now_Request_Count__c += 1;
					logList.add(RTL_Utility.InsertErrorTransactionWithServiceName(name,'',UserInfo.getName(),null,callmenowObj.response.message,'','CallMeNow-Lead',false));
				}
			}

			referralList.add(referral);
		}
	}
	
	global void finish(Database.BatchableContext BC) {

		if(logList.size() > 0){
			insert logList;
		}
		if(referralList.size() > 0){
			update referralList;
		}
		
	}
	
}