@isTest
public class ChangeOwnerAutoRecallBatchTest {
    public static final Datetime batchDate = datetime.now();
    // public static final Id RPSPEC_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB Commercial Report Specialist Profile' LIMIT 1].Id;
    // public static final Id TH_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB Team Head Profile' LIMIT 1].Id;
    // public static final Id BDM_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB BDM Profile' LIMIT 1].Id;

    public static final Id SESM_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB SE Sales Management Profile' LIMIT 1].Id;
    public static final Id SEZM_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB SE Zone Manager Profile' LIMIT 1].Id;
    public static final Id BDM_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB BDM Profile' LIMIT 1].Id;


    static{
        TestUtils.createAppConfig();

        Branch_and_zone__c bz = new Branch_and_zone__c();
        bz.Branch_Code__c = '001';
        insert bz;

        List<AppConfig__c> apps = new  List<AppConfig__c>();
        
        AppConfig__c Aconfig = new AppConfig__c();
        Aconfig.Name = 'Auto Recall Change Owner Approval flow';
        Aconfig.Value__c = '7';        
        apps.add(Aconfig);

        Aconfig = new AppConfig__c();
        Aconfig.Name = 'Auto Reassign Current Approver';
        Aconfig.Value__c = '2';        
        apps.add(Aconfig);

        insert apps;

        // UserRole ur = new UserRole();
        // ur.name = 'CSBO';
        // insert ur;

        List<User> userlst = new List<User>();

        User User01 = RTL_TestUtility.createUsers(1, 'testuser', 'testuser', 'testuser@tmb.com.test', false).get(0);
		User01.ProfileId = SESM_PROFILE_ID;
		User01.isActive = true;
		User01.RTL_Branch_Code__c = bz.Branch_Code__c;
		User01.Employee_ID__c = '111111';
		User01.Zone__c = 'SE-ChiangMai';
		User01.Zone_Code__c = '9999';
		User01.Region__c = 'SE1-North1';
		User01.Region_Code__c = '1111';
        User01.Segment__c = 'SE';
        userlst.add(User01);

        User User05 = RTL_TestUtility.createUsers(1, 'testuser5', 'testuser5', 'testuser5@tmb.com.test', false).get(0);
		User05.ProfileId = SEZM_PROFILE_ID;
		User05.isActive = true;
		User05.RTL_Branch_Code__c = bz.Branch_Code__c;
		User05.Employee_ID__c = '555555';
		User05.Zone__c = 'SE-ChiangMai';
		User05.Zone_Code__c = '8888';
		User05.Region__c = 'SE1-North1';
		User05.Region_Code__c = '2222';
        User05.Segment__c = 'SE';
        // User05.UserRoleId = ur.id;
        userlst.add(User05);

        insert userlst;

        User User04 = RTL_TestUtility.createUsers(1, 'testuser4', 'testuser4', 'testuser4@tmb.com.test', false).get(0);
		User04.ProfileId = BDM_PROFILE_ID;
		User04.isActive = true;
		User04.RTL_Branch_Code__c = bz.Branch_Code__c;
		User04.Employee_ID__c = '444444';
		User04.Zone__c = 'SE-ChiangMai';
		User04.Zone_Code__c = '9999';
		User04.Region__c = 'SE1-North1';
		User04.Region_Code__c = '1111';
        User04.Segment__c = 'SE';
        
        User04.ManagerId = User05.Id;
        
        insert User04;

        List<Account> existingAccts = TestUtils.createAccounts(1, 'name', 'Existing Customer', false);

        for (Account acct : existingAccts) {
            acct.OwnerId = User04.Id;
            acct.Account_Type__c = 'Existing Customer';
            acct.Stamp_Owner_Region_Group__c = 'SE1-North1';
            acct.Stamp_Owner_Region_Code_Group_Code__c = '1111';
            acct.Stamp_Owner_Zone_Team__c = 'SE-ChiangMai';
            acct.Stamp_Owner_Zone_Code_Team_C__c = '9999';
            acct.Stamp_Owner_Segment__c = 'SE';
            acct.Segment_crm__c = '4';
        }
        insert existingAccts;

        Change_Owner_Approval_History__c changeOwnerHistory = new Change_Owner_Approval_History__c();
        changeOwnerHistory.Account__c = existingAccts[0].Id;
        changeOwnerHistory.Approver1__c = User04.Id;
        changeOwnerHistory.Approver1_Profile__c = User04.Profile.Name;
        changeOwnerHistory.Approver2__c = User01.Id;
        changeOwnerHistory.Approver2_Profile__c = User01.Profile.Name;
        changeOwnerHistory.Change_to_owner__c = User05.Id;
        changeOwnerHistory.Current_Owner__c = existingAccts[0].OwnerId; 
        changeOwnerHistory.Change_Owner_Remark__c = 'preFixremark' + ' : ' ;

        changeOwnerHistory.Change_Owner_Approval_Status__c = 'Pending';
        changeOwnerHistory.Approval_Step_Start_Wait_Time__c = batchDate;
        changeOwnerHistory.Approval_Start_Wait_Time__c = batchDate;

        insert changeOwnerHistory;

        changeOwnerHistory = [SELECT Id, Change_to_owner__c, Change_to_owner__r.Segment__c, Account__c, Account__r.Owner.Segment__c , Account__r.Core_Banking_Suggested_Segment__c, Approver1__c, Approver2__c, Approver3__c, Approver4__c FROM Change_Owner_Approval_History__c WHERE Id = :changeOwnerHistory.Id];

        System.debug('changeOwnerHistory : ' + changeOwnerHistory);
        System.debug('changeOwnerHistory.Change_to_owner__c : ' + changeOwnerHistory.Change_to_owner__c);
        System.debug('changeOwnerHistory.Account__c : ' + changeOwnerHistory.Account__c);

        System.debug('changeOwnerHistory.Change_to_owner__r.Segment__c : ' + changeOwnerHistory.Change_to_owner__r.Segment__c);
        System.debug('changeOwnerHistory.Account__r.Owner.Segment__c : ' + changeOwnerHistory.Account__r.Owner.Segment__c);
        System.debug('changeOwnerHistory.Account__r.Core_Banking_Suggested_Segment__c : ' + changeOwnerHistory.Account__r.Core_Banking_Suggested_Segment__c);
        System.debug('changeOwnerHistory.Approver1__c : ' + changeOwnerHistory.Approver1__c);
        System.debug('changeOwnerHistory.Approver2__c : ' + changeOwnerHistory.Approver2__c);
        System.debug('changeOwnerHistory.Approver3__c : ' + changeOwnerHistory.Approver3__c);
        System.debug('changeOwnerHistory.Approver4__c : ' + changeOwnerHistory.Approver4__c);

        try{
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setObjectId(changeOwnerHistory.Id);
            req.setSubmitterId(User05.Id);
            Approval.ProcessResult result = Approval.process(req);
            //Approval.unlock(accId);
        }
        catch (Exception ex){
            System.debug(ex.getMessage());
            System.debug('Exception caught');
        }
    }

    @isTest
    public static void testConstructor() {
        
        Test.startTest();
        Database.executeBatch(new ChangeOwnerAutoRecallBatch());
        Test.stopTest();
    }

    @isTest
    public static void testRecall() {
        
        Test.startTest();
        Database.executeBatch(new ChangeOwnerAutoRecallBatch(batchDate.month(), batchDate.day() + 20));
        Test.stopTest();
    }
    
    @isTest
    public static void testEscalate() {
        
        Test.startTest();
        Database.executeBatch(new ChangeOwnerAutoRecallBatch(batchDate.month(), batchDate.day() + 6));
        Test.stopTest();
    }

    @isTest static void testschedule() {
        User u4 = [SELECT Id, Name, Profile.Name, ManagerId FROM User WHERE Email = 'testuser4@tmb.com.test' AND ProfileId = :BDM_PROFILE_ID LIMIT 1];

        Test.startTest();
        
        String jobId = System.schedule('test',  '0 0 0 15 3 ? *', new ChangeOwnerAutoRecallBatchScheduled());

        Map<Id, User> mapUser = ChangeOwnerAutoRecallBatchUtil.mapUser;
        List<String> specialProfileToIgnore = ChangeOwnerAutoRecallBatchUtil.specialProfileToIgnore;

        User u = ChangeOwnerAutoRecallBatchUtil.getNextApprover(u4);
        
        Test.stopTest();
    }

}